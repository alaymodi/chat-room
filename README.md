# 61C Chatroom



This project is to construct a simple chatroom in C. It consists of two main parts: a server and a client both built in C. The communication is implemented via TCP sockets using the C socket interface. This is already implemented for you and is not the focus of this project (these are covered in both cs 168 and cs 162). Instead the focus of this project will be interacting with C strings, interacting with files, dealing with C memory management, interacting with the C standard library, and completing something relatively cool. To do this you will need to make modifications to both the client and the server.

## Background

Despite the goals of this project being oriented around familiarity working with C it is also necessary to understand how the server and client work. Additionally it may be helpful to explain how the existing starter code works. If these portions outside the course interest you, feel free to read about them. However they will be marked OPTIONAL as they are not necessary to complete the project.

### Sockets OPTIONAL

Sockets are an abstraction for sending information between two processes. In this model a socket can be thought of as a queue where messages are sent by one user and then received by another user. In this way we can communicate between programs, between computers, or even across the world. In particular we will communicate using the client and server model. In this model a single server exists at a recognizable address and clients connect to this server. Connection is done using a protocol, in our case TCP (which you can learn more about in cs 161, cs 162, or cs 168). The key properties of TCP are that they produce in order, reliable communication. Additionally we will be communicating over IPv4, the IP address with which you are probably familiar. Conceptually TCP and IP can be thought of as together providing the means to specify where to send data. The IP address provides a location to send the data and TCP provides a port which can be tied to a particular process (you can think of this as being similar to specifying your address as the IP address and your apartment number as the port).

### Interfacing with Sockets OPTIONAL

What makes sockets so useful of an abstraction is that we can treat them exactly like files, well lower level files. A socket's information in c is stored as a file descriptor (which is an integer). A file descriptor is just any integer that is used to describe a particular file. As a result there are two primary functions the start codes uses to interact with sockets. The first is:

`ssize_t read (int fd, void *buf, size_t count)`

Which takes in a file descriptor you want to read from, a location to place the data, and the number of bytes you want to read and returns the number of bytes successfully read while placing the data read in `buf` (it returns 0 if the connection closes).

The second function you will need is:

`ssize_t write (int fd, void *buf, size_t count)`

which works similarly to read except it writes data from `buf`. If you are interested in learning more about either function you can type **man 2 read** or **man 2 write** into your linux terminal for a full description. Additionally for man C functions the command **man SOME\_NUMBER FUNCTION\_NAME** can be used to get information about that function from terminal. The number that needs to specified depends on how many linux manuals exist under that name.

### Server
When launching a server it is necessary to have a known ip address and port for clients to connect to. To accomplish this you will use your own machine to host the server and will also run your clients on the same machine. For grading purposes we will do the same thing on a hive machine. The server will run on your localhost (IP address 127.0.0.1) on a port of your specification (you should pick a port between 10000 and 60000, since most lower numbered ports are already allocated) and handle communication from all clients.
You can run the server as follows.
```
./server 10000
```

If you try to run the server immediately after disconnecting it you may receive a message that says:

**Unable to create server socket**

This simply means the port is not yet available for use and you should choose another port. When we grade your project we will avoid this by choosing a random port from among 50,000.


### Client
The client will connect to the server and have to be on the same port as the server. Run the client as follows.
```
./client NAME 127.0.0.1 10000
```
Your client will exit immediately if there is no server running at that port or address.

### Putting it together
Using three separate terminal windows, run the following commands, in this order.
Terminal 1 (Server):
```
make build
./server 10000
```
Terminal 2 (Client 1):
```
./client NAME1 127.0.0.1 10000
```
Terminal 3 (Client 2):
```
./client NAME2 127.0.0.1 10000
```
`make build` only needs to be run once, so we run it in the server window, the first to be used.

## Behavior

The first step to hosting the chatroom is to start up a server and begin having clients connect to it. Once a client connects to the server it will immediately send the NAME terminated by a newline. This will cause the server to print

\"NAME has joined\"

Every message in this project output by either the server or the client will be terminated by a newline, including those print by the server. This is necessary because Sockets are simply streams of bytes so some character needs to be sent to denote the end of a message. We will be using the \'\\n\' to do this, so all messages must end in a newline. Additionally, for the purposes of this project you can assume that all tests will involved unique names. You can also assume that all names will have the following specifications:

  * All names consist of at least one character where each character is from the set of characters \[A-Za-z0-9\_\]

  * All names have a maximum length of 251

Now that the client is connected to the server it will send messages to the server. There are two types of messages a client can send:

  1. A normal text message. This message should be printed by the server

  2. A command message (see the section below for a more complete description)/

For any message the server should first print out the message received. With a normal text message the server will then send it to all other users, unless a command alters this behavior by first appendingthe user's name and a \':\' to the front. To simplify the project we will set a limit on the maximum size of a message. The contents of a message will be set at a maximum of 771 characters. Additionally the first byte received from the server will always be an indication of the type of message received from the server and not part of the actual message. These options are:

  1. A message to exit.

  2. A normal message that the client should output.



```



  * ```commands.c```
    * This file contains functions that will be used to handle command behavior. The commands are described below in the commands sections.
    * You will be responsible for error checks, functionality, and ensuring that all memory is properly freed. You should **NOT** need to modify the messages being sent. If you implemented the rest of the function correctly the messages sent should be correct. This should be completed **FOURTH**.
    * The commands that you will need to modify functionality for will be:
      * set\_nickname
      * clear\_nickname
      * rename
      * mute
      * unmute
      * show\_status





